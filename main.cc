#include <cstdio>
#include <iostream>
#include <blaze/Math.h>


#include <cstdlib>
#include <ctime>
#include "neural_network.hh"


int main(int argc, char *argv[]) {

    srand(time(NULL));

    int entry_size = 2;

    Neural_Network nn(5,entry_size);

    blaze::DynamicVector<double> entree;
    for(int k=0;k<1000;++k){
        entree = blaze::DynamicVector<double> ( entry_size, 0.0D );
        entree = map( entree, []( double d ) { return ( rand()/(double)RAND_MAX ) * 2 - 1; } );
        entree = normalize(entree);
        //nn.debug();
        //std::cout << "entree =\n" << entree <<"\n";
        blaze::DynamicVector<double> sortie;
        if(entree[0]>entree[1]){
            sortie = blaze::DynamicVector<double> ( {1.0,0.0});
        }
        else{
            sortie = blaze::DynamicVector<double> ( {0.0,1.0});
        }
        nn.learn(entree,sortie);
    }

    nn.debug();

    entree = blaze::DynamicVector<double> ( entry_size, 0.0D );
    entree = map( entree, []( double d ) { return ( rand()/(double)RAND_MAX ) * 2 - 1; } );
    entree = normalize(entree);

    std::cout << "entree =\n" << entree <<"\n";

    cout << "prediction...\n" << endl;
    blaze::DynamicVector<double> out = nn.predict(entree);
    cout << out << "\nEtat actuel : ";
    nn.debug();
    cout << "\nFinished !"<<endl;

}
