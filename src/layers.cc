#include "layers.hh"

#define LR 0.01

////////////////////////////////////////////////////////////////////////////////
//                               Input_Layer                                  //
////////////////////////////////////////////////////////////////////////////////

Input_Layer::Input_Layer(int size) {
    size = size;
    functions = std::vector<Fun*>(size, Fun::ID);
}

blaze::DynamicVector<double> Input_Layer::compute(blaze::DynamicVector<double> data){
    last_data = data;
    for(uint k = 0; k < data.size(); ++k){
        data[k] = functions[k]->apply_f(data[k]);
    }
    return next_layer->compute(data);
}

void Input_Layer::print(){
    std::cout << "This input layer is valid at address " << this << "\n";
}

void Input_Layer::set_next(Layer* l){
    next_layer = l;
}

////////////////////////////////////////////////////////////////////////////////
//                              Output_Layer                                  //
////////////////////////////////////////////////////////////////////////////////

Output_Layer::Output_Layer(int entry_size, int output_size, Layer* previous){
    size = output_size;
    biais = blaze::DynamicVector<double> ( output_size, 0.0D );
    biais = map( biais, []( double d ) { return ( rand()/(double)RAND_MAX ) * 2 - 1; } );

    linear_comp = blaze::DynamicMatrix<double> ( output_size, entry_size, 0.0D );
    linear_comp = map( linear_comp, []( double d ) { return ( rand()/(double)RAND_MAX ) * 2 - 1; } );

    post_treatment_fun = Vfun::SOFT;
    previous_layer = previous;
    previous_layer -> set_next(this);

}

Output_Layer::Output_Layer(int entry_size, int output_size, Vfun* function, Layer* previous){
        Output_Layer(entry_size,output_size,previous);
        post_treatment_fun = function;
}


blaze::DynamicVector<double> Output_Layer::compute(blaze::DynamicVector<double> data){
    last_data = data;
    data = (linear_comp * data) + biais;
    data = post_treatment_fun -> apply_f(data);
    return data;
}

void Output_Layer::backprop_error(blaze::DynamicVector<double> error){
    blaze::DynamicVector<double> data_input = (linear_comp * last_data) + biais;
    blaze::DynamicVector<double> dout_din = post_treatment_fun->apply_df(data_input);
    // Changing weights and biais
    blaze::DynamicMatrix<double> d_linear_comp(linear_comp);
    for(uint i = 0; i < linear_comp.rows(); ++i){
        for(uint j = 0; j < linear_comp.columns(); ++j){
            d_linear_comp(i,j) = (error[j]*dout_din[j]*last_data[i]);
            linear_comp(i,j) -= (LR * (error[j]*dout_din[j]*last_data[i]));
        }
    }

    blaze::DynamicVector<double> d_biais(biais);
    for(uint k = 0; k < biais.size(); ++k){
        biais[k] -= (LR * (error[k]*dout_din[k]));
        d_biais[k] = (error[k]*dout_din[k]);
    }

    cout << "DW = \n "<< d_linear_comp<< "\nDB = \n" << d_biais << "\n";
    // Computing data for next backprop
    blaze::DynamicVector<double> dE_doutprev(last_data);
    for(uint k = 0; k < dE_doutprev.size(); ++k){
        double sum = 0.0;
        for(uint i = 0; i  < dout_din.size(); ++i){
            sum += error[i]*dout_din[i]*linear_comp(k,i);
        }
        dE_doutprev[k] = sum;
    }
    previous_layer ->backprop_error(dE_doutprev);
}



void Output_Layer::print(){
    std::cout << "This output layer is valid at address " << this << "\n";
    std::cout << "The weights and biais are : \nW = " << linear_comp << "\nB = "<<biais <<"\n";
}

////////////////////////////////////////////////////////////////////////////////
//                               Core_Layer                                   //
////////////////////////////////////////////////////////////////////////////////

Core_Layer::Core_Layer(int entry_size, int output_size, Layer* previous){

    size = output_size;

    biais = blaze::DynamicVector<double> ( output_size, 0.0D );
    biais = map( biais, []( double d ) { return ( rand()/(double)RAND_MAX ) * 2 - 1; } );

    linear_comp = blaze::DynamicMatrix<double> ( output_size, entry_size, 0.0D );
    linear_comp = map( linear_comp, []( double d ) { return ( rand()/(double)RAND_MAX ) * 2 - 1; } );

    functions = std::vector<Fun*>(output_size, Fun::RELU);

    previous_layer = previous;
    previous_layer -> set_next(this);
}

Core_Layer::Core_Layer(int entry_size, std::vector<Fun*> functions_a, Layer* previous){
    functions = functions_a;
    int output_size = functions_a.size();
    Core_Layer(entry_size,output_size,previous);
}

blaze::DynamicVector<double> Core_Layer::compute(blaze::DynamicVector<double> data){
    last_data = data;
    data = (linear_comp * data) + biais;
    for(uint k = 0;k< data.size();++k){
        data[k] = functions[k]->apply_f(data[k]);
    }
    return next_layer->compute(data);
}

void Core_Layer::backprop_error(blaze::DynamicVector<double> dE_dout){
    blaze::DynamicVector<double> data_input = (linear_comp * last_data) + biais;
    blaze::DynamicVector<double> dout_din(data_input);
    for(uint k = 0;k< data_input.size();++k){
        dout_din[k] = functions[k]->apply_df(data_input[k]);
    }
    //Changing weights and biais
    blaze::DynamicMatrix<double> d_linear_comp(linear_comp);
    for(uint i = 0; i < linear_comp.rows(); ++i){
        for(uint j = 0; j < linear_comp.columns(); ++j){
            d_linear_comp(i,j) = (dE_dout[j]*dout_din[j]*last_data[i]);
            linear_comp(i,j) -= (LR * (dE_dout[j]*dout_din[j]*last_data[i]));
        }
    }
    blaze::DynamicVector<double> d_biais(biais);
    for(uint k = 0; k < biais.size(); ++k){
        d_biais[k] = dE_dout[k]*dout_din[k];
        biais[k] -= (LR * (dE_dout[k]*dout_din[k]));
    }

    //cout << "DW = \n "<< d_linear_comp<< "\nDB = \n" << d_biais << "\n";

    // Compute data for next backprop

    blaze::DynamicVector<double> dE_doutprev(last_data);
    for(uint k = 0; k < dE_doutprev.size(); ++k){
        double sum = 0.0;
        for(uint i = 0; i  < dout_din.size(); ++i){
            sum += dE_dout[i]*dout_din[i]*linear_comp(k,i);
        }
        dE_doutprev[k] = sum;
    }
    previous_layer ->backprop_error(dE_doutprev);

}

void Core_Layer::set_next(Layer* l){
    next_layer = l;
}

void Core_Layer::print(){
    std::cout << "This layer is valid Core Layer at address " << this << " with next layer :"<< next_layer <<"\n";
    std::cout << "The weights and biais are : \nW = " << linear_comp << "\nB = "<<biais <<"\n";
}
