#include "function.hh"
#include <cmath>

double relu(double x)
{
	return max(0.,x);
}

double drelu(double x)
{
	if (x < 0) {return 0;} else {return 1;}
}

double sig(double x)
{
	return (1 / (1 + exp(-x)));
}

double dsig(double x)
{
	double temp = exp(-x);
	return temp / (1 + temp)*(1 + temp);
}

double id (double x)
{
	return x;
}

double did(double x)
{
	return 1;
}

double Fun::apply_f(double x) const
{
	return (*fun)(x);
}


double Fun::apply_df(double x) const
{
	return (*dfun)(x);
}

Fun::Fun(Fct f, Fct df)
{
	fun = f;
	dfun = df;
}

Fun* Fun::RELU = new Fun(&relu, &drelu);
Fun* Fun::SIG = new Fun(&sig, &dsig);
Fun* Fun::ID = new Fun(&id,  &did);


/**************************************************************************/

blaze::DynamicVector<double> soft(blaze::DynamicVector<double> v)
{
	unsigned n = v.size();
	double tot = 0.;
	for (unsigned i = 0; i < n; ++i)
	{
		tot = tot + exp(v[i]);
	};
	for (unsigned i = 0; i < n; ++i)
	{
		v[i] = exp(v[i]) / tot;
	};
	return v;
}

blaze::DynamicVector<double> dsoft(blaze::DynamicVector<double> v)
{
	unsigned n = v.size();
	double tot = 0.;
	for (unsigned i = 0; i < n; ++i)
	{
		tot = tot + exp(v[i]);
	};
	for (unsigned i = 0; i < n; ++i)
	{
		v[i] = (exp(v[i]) * (tot - exp(v[i]))) / (tot * tot);
	};
	return v;
}

blaze::DynamicVector<double> Vfun::error(blaze::DynamicVector<double> y, blaze::DynamicVector<double> out)
{
	blaze::DynamicVector<double> res(y.size());
	for(uint k = 0; k < y.size();++k){
		res[k] = -1.0 * ( (y[k]*(1.0/out[k])) + ((1.0-y[k])*(1.0/(1.0-out[k]))) );
	}
	return res;
}

double Vfun::centropy(blaze::DynamicVector<double> y, blaze::DynamicVector<double> out)
{
	double sum = 0.0;
	for(uint k = 0; k < y.size();++k){
		sum -= ((y[k]*log(out[k])) + ((1-y[k])*(log(1-out[k]))));
	}
	return sum;
}

blaze::DynamicVector<double> Vfun::apply_f(blaze::DynamicVector<double> v)
{
	return (*fun)(v);
}

blaze::DynamicVector<double> Vfun::apply_df(blaze::DynamicVector<double> v)
{
	return (*dfun)(v);
}

Vfun::Vfun(FctS f, FctS df)
{
	fun = f;
	dfun = df;
}

Vfun* Vfun::SOFT = new Vfun(&soft, &dsoft);
