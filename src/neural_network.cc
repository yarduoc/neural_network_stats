
#include "neural_network.hh"
#include "layers.hh"
#include "function.hh"

Neural_Network::Neural_Network(int n_layer, int layers_size){
    std::cout << "Creating input Layer" << "...\n";
    layers.push_back(new Input_Layer(layers_size));
    for(int k = 2; k < n_layer; ++k){
        std::cout << "Creating Layer number " << k << "...\n";
        layers.push_back(new Core_Layer(layers_size,layers_size,*(layers.end()-1)));
    }
    std::cout << "Creating output Layer" << "...\n";
    layers.push_back(new Output_Layer(layers_size,layers_size,*(layers.end()-1)));
}


blaze::DynamicVector<double> Neural_Network::predict(blaze::DynamicVector<double> data){
    blaze::DynamicVector<double> output = layers[0] -> compute(data);
    return output;
}


void Neural_Network::learn(blaze::DynamicVector<double> data, blaze::DynamicVector<double> expected){
    blaze::DynamicVector<double> prediction = predict(data);
    //double erreurcen = Vfun::centropy(expected,prediction);
    blaze::DynamicVector<double> erreur = Vfun::error(expected,prediction);
    //cout << "Sortie : " << prediction << endl;
    //cout << "Erreur : " << erreurcen << endl;
    (*(layers.end()-1))->backprop_error(erreur);

}

void Neural_Network::debug(){
    for(uint k = 0;k<layers.size();++k){
        layers[k]->print();
    }
}

std::vector<Layer*> Neural_Network::get_layers()
{
	return layers;
}
