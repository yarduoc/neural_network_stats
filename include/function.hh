#ifndef FUNCTIONS
#define FUNCTIONS

#include <iostream>
#include <blaze/Math.h>

using namespace std;

typedef double (*Fct)(double);

class Fun
{
private:
	Fct fun;
	Fct dfun;
public:
	double apply_f(double x) const;
	double apply_df(double x) const;
	Fun(Fct f, Fct df);

	static Fun* RELU;
	static Fun* SIG;
	static Fun* ID;
};


typedef blaze::DynamicVector<double> (*FctS)(blaze::DynamicVector<double>);

class Vfun
{
private:
	FctS fun;
	FctS dfun;
public:
	blaze::DynamicVector<double> apply_f(blaze::DynamicVector<double> v);
	blaze::DynamicVector<double> apply_df(blaze::DynamicVector<double> v);
	Vfun(FctS f, FctS df);

	static Vfun* SOFT;
	static blaze::DynamicVector<double> error(blaze::DynamicVector<double> y, blaze::DynamicVector<double> out);
	static double centropy(blaze::DynamicVector<double> y, blaze::DynamicVector<double> out);
};

//blaze::DynamicVector<double> compute_error(unsigned n, blaze::DynamicVector<double> y, blaze::DynamicVector<double> out);

#endif /* end of include guard: FUNCTIONS */
