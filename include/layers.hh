#ifndef LAYERS
#define LAYERS

#include <vector>
#include <blaze/Math.h>
#include "function.hh"


////////////////////////////////////////////////////////////////////////////////
//                                  Layer                                     //
////////////////////////////////////////////////////////////////////////////////


class Layer {

    protected :
        std::vector<Fun*> functions;
        int size;
        blaze::DynamicVector<double> last_data;

    public :
        virtual blaze::DynamicVector<double> compute(blaze::DynamicVector<double> data){};
        virtual void backprop_error(blaze::DynamicVector<double> error){cout << "backprop finished\n";};

        virtual void print(){std::cout<<"Virtual Layer \n";};
        virtual void set_next(Layer* l){};
};

////////////////////////////////////////////////////////////////////////////////
//                               Input_Layer                                  //
////////////////////////////////////////////////////////////////////////////////


class Input_Layer : public Layer{

    private :
        Layer *next_layer;

    public :
        Input_Layer(int size);
        blaze::DynamicVector<double> compute(blaze::DynamicVector<double> data);
        void print();
        void set_next(Layer* l);
};

////////////////////////////////////////////////////////////////////////////////
//                              Output_Layer                                  //
////////////////////////////////////////////////////////////////////////////////


class Output_Layer : public Layer {

    private :
        blaze::DynamicMatrix<double> linear_comp;
        blaze::DynamicVector<double> biais;

        Layer *previous_layer;
        Vfun *post_treatment_fun;

    public :
        Output_Layer(int entry_size, int output_size, Layer* previous);
        Output_Layer(int entry_size, int output_size, Vfun* function, Layer* previous);

        blaze::DynamicVector<double> compute(blaze::DynamicVector<double> data);
        void backprop_error(blaze::DynamicVector<double> data);

        void print();

};

////////////////////////////////////////////////////////////////////////////////
//                               Core_Layer                                   //
////////////////////////////////////////////////////////////////////////////////

class Core_Layer : public Layer{

    protected :
        blaze::DynamicMatrix<double> linear_comp;
        blaze::DynamicVector<double> biais;

        Layer *previous_layer, *next_layer;

    public :
        Core_Layer(int entry_size, int output_size, Layer* previous);
        Core_Layer(int entry_size, std::vector<Fun*> functions, Layer* previous);

        blaze::DynamicVector<double> compute(blaze::DynamicVector<double> data);
        void backprop_error(blaze::DynamicVector<double> data);

        void set_next(Layer* l);
        void print();

};

#endif /* end of include guard: LAYERS */
