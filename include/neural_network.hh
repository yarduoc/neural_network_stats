#ifndef NEURALNETWORK
#define NEURALNETWORK

#include <vector>
#include <blaze/Math.h>
#include "layers.hh"
#include "function.hh"


class Neural_Network {

    private :
        std::vector<Layer*> layers;

    public :
        Neural_Network(std::vector<int> size);
        Neural_Network(int n_layer, int layers_size);
        Neural_Network(std::vector<int> size, std::vector<Fun*> functions);
        Neural_Network(std::vector<int> size, std::vector<std::vector<Fun*>> functions);
        //
        void learn(blaze::DynamicVector<double> data, blaze::DynamicVector<double> expected);
        blaze::DynamicVector<double> predict(blaze::DynamicVector<double> data);
		std::vector<Layer*> get_layers();
        //
        void debug();
};


#endif /* end of include guard: NEURALNETWORK */
